require('dotenv').config();
const express = require('express');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
//define options for swagger before app
const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Rent-A-Car-API",
            version: "1.0.0",
            description: "A simple express api that manages renting events for vehicles."
        },
        servers: [
            {
                url: "http://localhost:3000"
            }
        ]
    },
    apis: ["./routes/*.js"]
}
const specs = swaggerJsDoc(options);
const app = express();
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
//ROUTES
const vehicleTypeRoutes = require('./routes/vehicleTypes');
const customerRoutes = require("./routes/customers");
const vehicleRoutes = require("./routes/vehicles");
const rentalEventsRoutes = require('./routes/rentalEvents');
const loginRoute = require('./routes/login');
const logoutRoute = require('./routes/logout');
//Connect to database - on success start listening on port 3000
mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true })
.then(result => app.listen(3000))
.catch(err => console.log(err));
app.use("/api-docs",swaggerUI.serve,swaggerUI.setup(specs));
// Tell my server to use json 
app.use(express.json());
//tell my server to use cookie parser
app.use(cookieParser());
//use vehicleType routes
app.use('/vehicle-types', vehicleTypeRoutes);
//use customer routes
app.use('/customers', customerRoutes);
//use vehicle routes
app.use('/vehicles', vehicleRoutes);
//use rental events routes
app.use('/rental-events', rentalEventsRoutes);
//use login route
app.use('/login', loginRoute);
//use logout route
app.use('/logout', logoutRoute);
