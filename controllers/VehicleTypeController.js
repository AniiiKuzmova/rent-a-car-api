const VehicleType = require('../models/vehicleType');
const handleErrors = require('../middlewares/handleErrors');
//create controller-object to store methods for the routes
const VehicleTypeController = {
    //controller function for getting all vehicle-types
    getAllVehicleTypes: function(req,res) {
        VehicleType
        .find()
        .exec()
        .then(result => res.status(200).json(result))
        .catch(err => res.status(500).json({err}));
    },
    //controller-function for getting one vehicle type
    getOneVehicleType : function(req,res) {
        res.status(200).json(res.vehicleType);
    },
    //controller-function for creating vehicle type
    createVehicleType: function(req,res) {
        const newVehicleType = new VehicleType({
            name : req.body.name
        });
        newVehicleType.save()
        .then(result => {
            res.status(201).send(result);
        })
        .catch(err => {
            const er = handleErrors(err);
            res.status(400).json({er});
        });
    },
    //controller-function for updating vehicle type
    updateVehicleType: function(req,res) {
        //check if user has updated name field
        if(req.body.name != null) {
            res.vehicleType.name = req.body.name;
        }
        res.vehicleType.save()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            const er = handleErrors(err);
            res.status(400).json({er});
        });
    },
    //controller function for deleting vehicle type
    deleteVehicleType : function(req,res) {
        res.vehicleType.remove()
        .then(result => {
            res.status(200).json({message: "Successfully removed vehicle type"});
        })
        .catch(err => {
            res.status(500).json(err);
        });
    }
}
//export controller
module.exports = VehicleTypeController;