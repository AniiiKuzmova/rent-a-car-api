const RentalEvent = require('../models/rentalEvent');
const Vehicle = require('../models/vehicle');
const Customer = require('../models/customer');
const validations = require('../validations/rentalEventValidation');
const handleErrors = require('../middlewares/handleErrors');
//create controller-object to store methods for the routes
const RentalEventController = {
    //controller-function for getting all rental events
    getAllRentalEvents: function(req,res) {
        RentalEvent
        .find()
        .catch()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            res.status(500).json(err);
        });
    },
    getOneRentalEvent: function(req,res) {
        res.status(200).json(res.foundRentalEvent);
    },
    //controller-function for getting one rental event
    createRentalEvent:async function(req,res) {
        const {vehicle,customer,startDate,endDate} = req.body;
        try {
            const event = await RentalEvent.create({vehicle,customer,startDate,endDate});
            res.status(201).json(event);
        }catch(error) {
            res.json({ error: error.message });
        }
    },
    //controller function for updating rental event
    updateRentalEvent: function(req,res) {
        //Update form (edit) should send all data for a rental event
        const updatedRentalEvent = req.body;
        RentalEvent
        .findOneAndUpdate(req.params.id, updatedRentalEvent, { upsert: true, useFindAndModify: false, runValidators: true})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            res.status(400).json({ error: error.message });
        });
    },
    //controller-function for removing a rental event
    deleteRentalEvent: function(req,res) {
        res.foundRentalEvent.remove()
        .then(result => {
            res.status(200).json({message: "Successfully removed rental event."});
        })
        .catch(err => {
            res.status(500).json(err);
        });
    }
}
//export controller-object
module.exports = RentalEventController;