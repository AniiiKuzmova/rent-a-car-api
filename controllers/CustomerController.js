const Customer = require('../models/customer');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const handleErrors = require('../middlewares/handleErrors');
//create web token function
const myAge = 3*24*60*60;
const createWebToken = (id) => {
    return jwt.sign({id}, 'ani',
    {
        expiresIn: myAge
    });
}

//create controller-object to store methods for the routes
const CustomerController = {
    //controller-function for getting all customers
    getAllCustomers : function(req,res) {
        Customer.find()
        .then(result => {
            res.status(200).send(result);
        })
        .catch(err => {
            res.status(500).json(err);
        });
    },
    //controller-funtion for getting one customer
    getOneCustomer: function(req,res) {
        res.status(200).json(res.custy);
    },
    //controller function for creating a customer
    crateCustomer: async function(req,res) {
        const {fullname, email, password, phoneNumber} = req.body;
        try{
            const customer = await Customer.create({fullname, email, password, phoneNumber});
            const token = createWebToken(customer._id);
            res.cookie('jwt',token, {httpOnly:true, maxAge: myAge});
            res.status(201).json(customer);
        }catch(error) {
           const er =  handleErrors(error);
           res.status(400).json({er});
        }
    },
    //controller-function for updating a customer
    updateCustomer: function(req,res) {
        const newCusty = req.body;
        Customer.findByIdAndUpdate(req.params.id, newCusty, { upsert: true, useFindAndModify: false, runValidators: true, context: 'query'})
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            const er =  handleErrors(err);
            res.status(400).json({er});
        });
    },
    //controller function for deleting a customer
    deleteCustomer: function(req,res) {
        res.custy.remove()
        .then(result => {
            res.status(200).json({message : "Successfully removed customer."});
        })
        .catch(err => {
            res.status(500).json(err);
        });
    }
}
//export controller
module.exports = CustomerController;