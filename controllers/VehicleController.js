const Vehicle = require('../models/vehicle');
const handleErrors = require('../middlewares/handleErrors');
//create controller-object to store methods for the routes
const VehicleController = {
    //controller-function for getting all vehicles
    getAllVehicles : function(req,res) {
        Vehicle
        .find()
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            res.status(500).json({err});
        });
    },
    //controller function for getting one vehicle
    getOneVehicle: function(req,res) {
        res.status(200).json(res.foundVehicle);
    },
    //controller-function for creating a vehicle
    createVehicle: function(req, res) {
        const newVehicle = new Vehicle({
            vehicleType: req.body.vehicleType,
            brand: req.body.brand,
            model: req.body.model,
            constructionYear: req.body.constructionYear,
            fuelType: req.body.fuelType,
            numberOfSeats: req.body.numberOfSeats,
            picture: req.body.picture,
            pricePerDay: req.body.pricePerDay,
            count: req.body.count
        })
        newVehicle
        .save()
        .then(result => {
            res.status(201).json(result);
        })
        .catch(err => {
            const errors = handleErrors(err);
            res.status(400).json({errors});
        });
    },
    //controller-function for updating a vehicle
    updateVehicle: function(req,res) {
        const updatedProperties = req.body;
        Vehicle
        .findOneAndUpdate(req.params.id, updatedProperties, { upsert: true, useFindAndModify: false, runValidators: true})
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            const errors = handleErrors(err);
            res.status(400).json({errors});
        });
    },
    //controller-function for deleting a vehicle
    deleteVehicle: function(req,res) {
        res.foundVehicle.remove()
        .then(result => {
            res.status(200).json({message: "Successfully removed vehicle."});
        })
        .catch(err => {
            res.status(500).json(err);
        });
    }
}
//export contoller
module.exports = VehicleController;