const express = require('express');
const router = express.Router();
const Vehicle = require('../models/vehicle');
const jwt = require('jsonwebtoken');
const {requireAuth} = require('../middlewares/requireAuth');
const VehicleController = require('../controllers/VehicleController');
const getVehicle = require('../middlewares/getVehicle');
/**
 * @swagger
 *  components:
 *    schemas:
 *      Vehicle:
 *        type: object
 *        required:
 *          -brand
 *          -model
 *          -constructionYear
 *          -fuelType
 *          -numberOfSeats
 *          -picture
 *          -pricePerDay
 *          -count
 *          -vehicleType 
 *        properties:
 *          id: 
 *            type: string
 *            description: The auto-generated id of the vehicle document
 *          vehicleType:
 *            type: string
 *            description: id referencing a vehicle type document  
 *          brand:
 *            type: string
 *            description: The brand oof the vehicle
 *          model:
 *            type: string
 *            description: Model of the vehicle
 *          constructionYear:
 *            type: number
 *            description: Year in which the vehicle is constructed
 *          numberOfSeats:
 *            type: number
 *            description: Number of seats of the vehicle
 *          pricePerDay:
 *            type: number
 *            description: Daily price of the vehicle
 *          fuelType:
 *            type: string
 *            description: Fuel type for vehicle. It can be diesel,petrol,electric or hybrid
 *          picture:
 *            type: string
 *            description: Picture of the vehicle  
 *          count:
 *            type: number
 *            description: Number of vehicles         
 *          createdAt: 
 *            type: string
 *            description: Date on which the customer is created
 *          updatedAt:
 *            type: string
 *            description: Last date on which the customer has been updated
 */

/**
 * @swagger
 * tags:
 *   name: Vehicles
 *   description: Vehicles managing API  
*/

/**
 * @swagger
 *  /vehicles:
 *   get:
 *    summary: Returns a json object containing all vehicles
 *    tags: [Vehicles] 
 *    responses:
 *      200:
 *        description: The json object of the vehicles
 *        content: 
 *          application/json: 
 *            schema:
 *              type: object
 *              items: 
 *                $ref:'#/components/schemas/Vehicle' 
 *      500:                                                  
 *        description: Server error                                                      
*/
//get all vehicles
router.get('/', VehicleController.getAllVehicles);
/**
 * @swagger
 *  /vehicles/{id}:
 *   get:
 *    summary: Returns a vehicle with the passed id
 *    tags: [Vehicles]
 *    parameters: 
 *      -  in: path
 *         name: id
 *         schema: 
 *           type: string
 *         required: true
 *         description: The vehicle id
 *    responses:
 *      200:
 *        description: Vehicle by id
 *        content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/Vehicle'
 *      404:
 *        description: Vehicle was not found                     
*/
//get one vehicle
router.get('/:id', getVehicle, VehicleController.getOneVehicle);
/**
 * @swagger
 *  /vehicles:
 *   post:
 *     summary: Create new vehicle
 *     tags: [Vehicles]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Vehicle'
 *     responses:
 *       200:                    
 *         description: Vehicle was successfully created                    
 *         content:                    
 *           application/json:                         
 *             $ref: '#/components/schemas/Vehicle'                      
 *       400:                          
 *         description: Bad request                                                                      
*/
//create vehicle
router.post('/',requireAuth, VehicleController.createVehicle);
/**
 * @swagger
 *  /vehicles/{id}:
 *   patch:                 
 *     summary: Update a vehicle  by id                                        
 *     tags: [Vehicles]                      
 *     parameters:                               
 *      - in: path                                      
 *        name: id                                         
 *        schema:                         
 *          type: string                                         
 *        required: true                                  
 *        description: The vehicle id                              
 *     requestBody:                                     
 *        required: true                                      
 *        content:                                           
 *          application/json:                                             
 *            schema:                                            
 *              $ref: '#/components/schemas/Vehicle'                                                        
 *     responses:                                                             
 *        200:
 *          description: The vehicle was updated
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Vehicle'                                                                        
 *        404:
 *          description: The vehicle was not found                                                          
 *        500:                                                      
 *          description: server error                                                     
 *                                                                              
 *                                                                       
 *                                                   
*/
//update vehicle
router.patch('/:id',requireAuth, getVehicle, VehicleController.updateVehicle);
/**
 * @swagger
 *  /vehicles/{id}:
 *   delete:
 *    summary: Remove vehicle by id
 *    tags: [Vehicles]                                                                 
 *    parameters:                                                                         
 *     - in: path                                                                     
 *       name: id                                                                         
 *       schema:                                                                               
 *        type: string                                                                           
 *       required: true                                                                                           
 *       description: The vehicle id                                                                                              
 *    responses:                                                                                                             
 *       200:                                                                                                   
 *        description: The vehicle was deleted                                         
 *       404:                                                    
 *        description: The vehicle was not found
 *       500:                                                  
 *        description: Server error                                                                                                                                                             
*/
//delete vehicle
router.delete('/:id',requireAuth, getVehicle, VehicleController.deleteVehicle);
//export routes for vehicles
module.exports = router;