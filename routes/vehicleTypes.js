const express = require('express');
const router = express.Router();
const VehicleType = require('../models/vehicleType');
const VehicleTypeController = require('../controllers/VehicleTypeController');
const jwt = require('jsonwebtoken');
const {requireAuth} = require('../middlewares/requireAuth');
const getVehicleType = require('../middlewares/getVehicleType');
/**
 * @swagger
 *  components:
 *    schemas:
 *      VehicleType:
 *        type: object
 *        required:
 *          -name
 *        properties:
 *          id: 
 *            type: String
 *            description: The auto-generated id of the vehicle type document
 *          name:
 *            type: String
 *            description: The name of the vehicle type
 *          createdAt: 
 *            type: string
 *            description: Date on which the customer is created
 *          updatedAt:
 *            type: string
 *            description: Last date on which the customer has been updated        
 */

/**
 * @swagger
 * tags:
 *   name: Vehicle-Types
 *   description: Vehicle-types managing API  
*/

/**
 * @swagger
 *  /vehicle-types:
 *   get:
 *    summary: Returns a json object of all vehicle types
 *    tags: [Vehicle-Types] 
 *    responses:
 *      200:
 *        description: The json object of the vehicle types
 *        content: 
 *          application/json: 
 *            schema:
 *              type: object
 *              items: 
 *                $ref:'#/components/schemas/VehicleType'
 *      500:                                              
 *        description: Server error                                                            
*/

//get all vehicle types
router.get('/',VehicleTypeController.getAllVehicleTypes);
/**
 * @swagger
 *  /vehicle-types/{id}:
 *   get:
 *    summary: Get a vehicle type with the passed id
 *    tags: [Vehicle-Types]
 *    parameters: 
 *      -  in: path
 *         name: id
 *         schema: 
 *           type: string
 *         required: true
 *         description: The vehicle type id
 *    responses:
 *      200:
 *        description: Vehicle-type by id
 *        content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/VehicleType'
 *      404:
 *        description: Vehicle type was not found                     
*/
//Get one vehicle with specific id
router.get('/:id',getVehicleType, VehicleTypeController.getOneVehicleType);
/**
 * @swagger
 *  /vehicle-types:
 *   post:
 *     summary: Create new vehicle type
 *     tags: [Vehicle-Types]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/VehicleType'
 *     responses:
 *       200:                    
 *         description: Vehicle type was successfully created                    
 *         content:                    
 *           application/json:                         
 *             $ref: '#/components/schemas/VehicleType'                      
 *       400:                          
 *         description: Bad request                             
 *                                
 *                                           
*/
//create vehicle type
router.post('/',requireAuth, VehicleTypeController.createVehicleType);
/**
 * @swagger
 *  /vehicle-types/{id}:
 *   patch:                 
 *     summary: Update a vehicle type  by id                                        
 *     tags: [Vehicle-Types]                      
 *     parameters:                               
 *      - in: path                                      
 *        name: id                                         
 *        schema:                         
 *          type: string                                         
 *        required: true                                  
 *        description: The vehicle type id                              
 *     requestBody:                                     
 *        required: true                                      
 *        content:                                           
 *          application/json:                                             
 *            schema:                                            
 *              $ref: '#/components/schemas/VehicleType'                                                        
 *     responses:                                                             
 *        200:
 *          description: The vehicle type was updated
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/VehicleType'                                                                        
 *        404:
 *          description: The vehicle type was not found                                                          
 *        500:                                                      
 *          description: server error                                                     
 *                                                                              
 *                                                                       
 *                                                   
*/
//update  vehicle type
router.patch('/:id',requireAuth, getVehicleType, VehicleTypeController.updateVehicleType);
/**
 * @swagger
 *  /vehicle-types/{id}:
 *   delete:
 *    summary: Remove vehicle type by id
 *    tags: [Vehicle-Types]                                                                 
 *    parameters:                                                                         
 *     - in: path                                                                     
 *       name: id                                                                         
 *       schema:                                                                               
 *        type: string                                                                           
 *       required: true                                                                                           
 *       description: The vehicle type id                                                                                              
 *    responses:                                                                                                             
 *       200:                                                                                                   
 *        description: The vehicle type was deleted                                         
 *       404:                                                    
 *        description: The vehicle was not found
 *       500:                     
 *        description: Server error                                                                                                                               
*/
//delete a vehicle type
router.delete('/:id',requireAuth, getVehicleType, VehicleTypeController.deleteVehicleType);
//export router
module.exports = router;