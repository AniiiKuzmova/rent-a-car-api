const express = require('express');
const router = express.Router();
const Customer = require('../models/customer');
const CustomerController = require('../controllers/CustomerController');
const jwt = require('jsonwebtoken');
const {requireAuth} = require('../middlewares/requireAuth');
const getCustomer = require('../middlewares/getCustomer');
/**
 * @swagger
 *  components:
 *    schemas:
 *      Customer:
 *        type: object
 *        required:
 *          -fullname
 *          -email
 *          -password
 *          -phoneNumber
 *        properties:
 *          id: 
 *            type: string
 *            description: The auto-generated id of the cusstomer document
 *          fullname:
 *            type: string
 *            description: The fullname of the customer
 *          email:
 *            type: string
 *            description: Email addres of the customer
 *          password:
 *            type: string
 *            description: Password of the customer. After creating a user the password is hashed.
 *          phoneNumber:
 *            type: string
 *            description: Phone number of the customer
 *          createdAt: 
 *            type: string
 *            description: Date on which the customer is created
 *          updatedAt:
 *            type: string
 *            description: Last date on which the customer has been updated
 *        example:
 *          id: if34dhtWR45ds 
 *          fullname: Doctor Who 
 *          password: $2b$10$Glms/c9cLEQ6j6YWhlUe5O7OCatSy7qiIDo0PtS1mPPU4NklKLGrm
 *          email: dwWho@abv.bg
 *          phoneNumber: 0987654321 
 *          createdAt: 2021-07-29T09:22:06.799Z 
 *          updatedAt: 2021-07-29T09:48:05.128Z 
 * 
 */ 

/**
 * @swagger
 * tags:
 *   name: Customers
 *   description: Customers managing API  
*/

/**
 * @swagger
 *  /customers:
 *   get:
 *    summary: Returns an object with all customers
 *    tags: [Customers] 
 *    responses:
 *      200:
 *        description: The json object of the customers
 *        content: 
 *          application/json: 
 *            schema:
 *              type: object
 *              items: 
 *                $ref:'#/components/schemas/Customer'  
 *      500:                                                       
 *        description: Server error                                                       
 *                                                              
 *                                                             
 *                                                                    
*/
//Getting all customers
router.get('/', CustomerController.getAllCustomers);
/**
 * @swagger
 *  /customers/{id}:
 *   get:
 *    summary: Returns a customer matchiing the passed id
 *    tags: [Customers]
 *    parameters: 
 *      -  in: path
 *         name: id
 *         schema: 
 *           type: string
 *         required: true
 *         description: The customer id
 *    responses:
 *      200:
 *        description: Customer by id
 *        content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/Customer'
 *      404:
 *        description: Customer was not found 
*/
//Get one customer
router.get('/:id', getCustomer, CustomerController.getOneCustomer);
/**
 * @swagger
 *  /customers:
 *   post:
 *     summary: Creates new customer
 *     tags: [Customers]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Customer'
 *     responses:
 *       200:                    
 *         description: Customer was successfully created                    
 *         content:                    
 *           application/json:                         
 *             $ref: '#/components/schemas/Customer'                      
 *       400:                          
 *         description: Bad request                             
 *                                
 *                                           
*/
//Create customer - register
router.post('/', CustomerController.crateCustomer);
/**
 * @swagger
 *  /customers/{id}:
 *   patch:                 
 *     summary: Update a customer by id                                        
 *     tags: [Customers]                      
 *     parameters:                               
 *      - in: path                                      
 *        name: id                                         
 *        schema:                         
 *          type: string                                         
 *        required: true                                  
 *        description: The customer id                              
 *     requestBody:                                     
 *        required: true                                      
 *        content:                                           
 *          application/json:                                             
 *            schema:                                            
 *              $ref: '#/components/schemas/Customer'                                                        
 *     responses:                                                             
 *        200:
 *          description: The customer was updated
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Customer'                                                                        
 *        404:
 *          description: The customer was not found                                                          
 *        500:                                                      
 *          description: server error                                                     
 *                                                                              
 *                                                                       
 *                                                   
*/
//Update customer
router.patch('/:id',requireAuth, getCustomer, CustomerController.updateCustomer);
/**
 * @swagger
 *  /customers/{id}:
 *   delete:
 *    summary: Removes customer by id
 *    tags: [Customers]                                                                 
 *    parameters:                                                                         
 *     - in: path                                                                     
 *       name: id                                                                         
 *       schema:                                                                               
 *        type: string                                                                           
 *       required: true                                                                                           
 *       description: The customer id                                                                                              
 *    responses:                                                                                                             
 *       200:                                                                                                   
 *        description: The customer was deleted                                         
 *       404:                                                    
 *        description: The customer was not found 
 *       500:                                               
 *        description: Server error                                                   
 *                                                                                                                                                
*/
//Delete customer
router.delete('/:id',requireAuth, getCustomer, CustomerController.deleteCustomer);
//export router
module.exports = router;

