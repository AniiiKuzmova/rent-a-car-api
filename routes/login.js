const express = require('express');
const router = express.Router();
const Customer = require('../models/customer');
const jwt = require('jsonwebtoken');
const createWebToken = (id) => {
    return jwt.sign({id}, 'ani',
    {
        expiresIn: myAge
    });
}
const myAge = 3*24*60*60*1000;
/**
 * @swagger
 *  /login:
 *   post:
 *     summary: Log new customer
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Customer'
 *     responses:
 *       200:                    
 *         description: Logged in                    
 *         content:                    
 *           application/json:                         
 *             type: object                      
 *       400:                          
 *         description: Wrong data                                                                      
*/
//login route
router.post('/', async (req,res) => {
    const {email, password} = req.body;
    try {
        const user = await Customer.login(email,password);
        const token = createWebToken(user._id);
        res.cookie('jwt',token, {httpOnly:true, maxAge: myAge});
        res.status(200).json({user: user._id});
    }catch(error) {
        res.status(400).json({msg: error.message});
    }
});
//export the router
module.exports = router;
