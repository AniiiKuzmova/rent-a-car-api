const express = require('express');
const router = express.Router();
const cookieParser = require('cookie-parser');
const {requireAuth} = require('../middlewares/requireAuth');
/**
 * @swagger
 *  /logout:
 *    get:
 *      summary: Log a user out  
 *      responses:        
 *        200:               
 *          description: Return successfull message for loging a user out                  
 *        401:                  
 *          description: Not logged or registered                                    
*/
//router for logging user's out
router.get('/', requireAuth, (req, res) => {
    res.cookie('jwt', '', {maxAge: 1});
    //redirect user to home page
    res.status(200).json({message: 'Logged user out!'})
});
//export router
module.exports = router;