const express = require('express');
const router = express.Router();
const RentalEventController = require('../controllers/RentalEventController');
const {requireAuth} = require('../middlewares/requireAuth');
const getRentalEvent = require('../middlewares/getRentalEvent');
/**
 * @swagger
 *  components:
 *    schemas:
 *      RentalEvent:
 *        type: object
 *        required:
 *          -customer
 *          -vehicle
 *          -startDate
 *          -endDate
 *        properties:
 *          id:
 *            type: string
 *            description: The auto-generated of the rental event
 *          customer:
 *            type: string 
 *            description: String containig the id that references a customer document 
 *          vehicle: 
 *            type: string 
 *            description: String containing the id that references a vehicle document 
 *          startDate: 
 *            type: string 
 *            description: Start date of the rentel event 
 *          endDate: 
 *            type: string 
 *            description: End date of the rental event 
 *          price: 
 *            type: number 
 *            description: Price for the whole rental event   
 */ 

/**
 * @swagger
 * tags:
 *   name: Rental-Events
 *   description: Rental-events managing API  
*/

/**
 * @swagger
 *  /rental-events:
 *   get:
 *    summary: Returns a json object of all rental-events
 *    tags: [Rental-Events] 
 *    responses:
 *      200:
 *        description: The json object of the rental-event
 *        content: 
 *          application/json: 
 *            schema:
 *              type: object
 *              items: 
 *                $ref:'#/components/schemas/RentalEvent'  
 *      500:                                                   
 *        description: Server error                                                 
*/
//get all rental events
router.get('/', RentalEventController.getAllRentalEvents);
/**
 * @swagger
 *  /rental-events/{id}:
 *   get:
 *    summary: Returns an object containing the rental event with the passed id
 *    tags: [Rental-Events]
 *    parameters: 
 *      -  in: path
 *         name: id
 *         schema: 
 *           type: string
 *         required: true
 *         description: The rental event id
 *    responses:
 *      200:
 *        description: Rental event by id
 *        content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/RentalEvent'
 *      404:
 *        description: Rental event was not found                     
*/
//get one event
router.get('/:id',getRentalEvent, RentalEventController.getOneRentalEvent);
/**
 * @swagger
 *  /rental-events:
 *   post:
 *     summary: Create new rental event
 *     tags: [Rental-Events]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/RentalEvent'
 *     responses:
 *       200:                    
 *         description: Rental event was successfully created                    
 *         content:                    
 *           application/json:                         
 *             $ref: '#/components/schemas/RentalEvent'                      
 *       400:                          
 *         description: Bad request                                                                       
*/
//create event - rent a car
router.post('/',requireAuth,getRentalEvent, RentalEventController.createRentalEvent);
/**
 * @swagger
 *  /rental-events/{id}:
 *   patch:                 
 *     summary: Update a rental event by id                                        
 *     tags: [Rental-Events]                      
 *     parameters:                               
 *      - in: path                                      
 *        name: id                                         
 *        schema:                         
 *          type: string                                         
 *        required: true                                  
 *        description: The rental event id                              
 *     requestBody:                                     
 *        required: true                                      
 *        content:                                           
 *          application/json:                                             
 *            schema:                                            
 *              $ref: '#/components/schemas/RentalEvent'                                                        
 *     responses:                                                             
 *        200:
 *          description: The rental event was updated
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/RentalEvent'                                                                        
 *        404:
 *          description: The rental event was not found                                                          
 *        500:                                                      
 *          description: server error                                                     
 *                                                                              
 *                                                                       
 *                                                   
*/
//update
router.patch('/:id',requireAuth,getRentalEvent, RentalEventController.updateRentalEvent);
/**
 * @swagger
 *  /rental-events/{id}:
 *   delete:
 *    summary: Remove rental event by id
 *    tags: [Rental-Events]                                                                 
 *    parameters:                                                                         
 *     - in: path                                                                     
 *       name: id                                                                         
 *       schema:                                                                               
 *        type: string                                                                           
 *       required: true                                                                                           
 *       description: The rental event id                                                                                              
 *    responses:                                                                                                             
 *       200:                                                                                                   
 *        description: The rental event was deleted                                         
 *       404:                                                    
 *        description: The rental event was not found
 *       500:                                                
 *        description: Server error                                                                                                                                                                
*/
//delete rental event
router.delete('/:id',requireAuth,getRentalEvent ,RentalEventController.deleteRentalEvent);
//export routes for rental event
module.exports = router;