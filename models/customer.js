const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const {isEmail} = require('validator');
const bcrypt = require('bcrypt');
//create schema for customer
const customerSchema = new Schema({
    fullname : {
        type: String,
        required: [true, 'Please enter a name.']
    },
    email: {
        type : String,
        required : [true,'Please enter an email.'],
        unique: true,
        validate: [isEmail, 'Please enter a valid email.']
    },
    phoneNumber : {
        type : String,
        required : [true, 'Please enter a phone number.'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'Please enter a password.'],
        minlength: [8, 'Minimum password length is 8 characters.']
    }
}, {timestamps: true});
//plugin uniqueValidator - validates unique values
customerSchema.plugin(uniqueValidator);
//take the password value and generate password
customerSchema.pre('save', async function(next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password,salt);
    next();
});
//Static method to login customers
customerSchema.statics.login = async function(email, password) {
    const user = await this.findOne({email});
    if(user) {
       const auth = await bcrypt.compare(password, user.password);
       if(auth) {
           return user; 
       }
       throw Error('Incorrect password.');
    }
    throw Error('Incorrect email.');
}
//Create model based on the schema and exports
const Customer = mongoose.model('Customer', customerSchema);
module.exports = Customer;