const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {calculateDateBtweenDates,calculatePrice} = require('../validations/rentalEventValidation');
//create schema for rental types
const rentalEventSchema = new Schema({
    price: {
        type: Number
    },
    vehicle: {
        type: Schema.Types.ObjectId,
        ref: 'Vehicle',
        required: true,
    },
    customer: {
        type: Schema.Types.ObjectId,
        ref: 'Customer',
        required: true,
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true,
    }
});
//pre hook funtion
rentalEventSchema.pre('save', async function(next){
    const customer = await mongoose.models.Customer.findById(this.customer).exec();
    const vehicle = await mongoose.models.Vehicle.findById(this.vehicle).exec();
    //check if customer and vehicles exists
    if(!customer) {
       next(new Error('Customer does not exists'));
    }
    if(!vehicle) {
        next(new Error('Vehicle does not exists'));
    }
    const dateRangeDiff = calculateDateBtweenDates(this.endDate,this.startDate);
    if(dateRangeDiff < 0) {
        next(new Error('Start Date and End Date are not in the correct format.'));
    }
    this.price = await calculatePrice(dateRangeDiff,customer,vehicle);
    next();
});
//before update
rentalEventSchema.pre('findOneAndUpdate', async function(next) {
    const customer = await mongoose.models.Customer.findById(this._update.customer).exec();
    const vehicle = await mongoose.models.Vehicle.findById(this._update.vehicle).exec();
  
    //check if customer and vehicles exists
    if(!customer) {
        next(new Error('Customer does not exists'));
     }
     if(!vehicle) {
       next(new Error('Vehicle does not exists'));
     }
    const dateRangeDiff = calculateDateBtweenDates(this._update.endDate,this._update.startDate);
    if(dateRangeDiff < 0) {
        next(new Error('Start Date and End Date are not in the correct format.'));
    }
    const price = await calculatePrice(dateRangeDiff,customer,vehicle);
    await this.updateOne({},{}).set('price', price);
});
//create and export model from schema
const RentalEvent = mongoose.model('Rentalevent', rentalEventSchema);
module.exports = RentalEvent;