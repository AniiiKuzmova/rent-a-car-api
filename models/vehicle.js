const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validations = require('../validations/vehicleValidations');
//create schema for vehicles
const vehicleSchema = new Schema({
    vehicleType : {
        type: Schema.Types.ObjectId,
        ref: 'Vehicletype',
        required: true,
        validate: [validations.checkVehicleType, "Vehicle Type with the passed id does not exists."]
    },
    brand : {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    constructionYear : {
        type: Number,
        reuired: true
    },
    fuelType: {
        type: String,
        required: true,
        validate: [validations.validateFuelType, "Fuel type must be petrol,diesel,electric or hybrid."]
    },
    numberOfSeats: {
        type: Number,
        required: true,
        min: [1, 'Number of seats must 1 or more']
    },
    picture: {
        type: String,
        required: true
    },
    pricePerDay: {
        type: Number,
        required: true
    },
    count: {
        type: Number,
        required: true,
        min: [0, 'Count of vehicle cannot be less than 0']
    } 
}, {timestamps: true});
//create and export model for vehicles
const Vehicle = mongoose.model('Vehicle', vehicleSchema);
module.exports = Vehicle;
