const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
//create chema for vehicle type
const vehicleTypeSchema = new Schema({
    name : {
        type : String,
        required : [true, 'Please enter a name.'],
        unique: true 
    }
}, {timestamps: true});
//validate unique values
vehicleTypeSchema.plugin(uniqueValidator);
//create and export model for vehicle type
const VehicleType = mongoose.model('Vehicletype', vehicleTypeSchema);
module.exports = VehicleType;
