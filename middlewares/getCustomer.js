const Customer = require('../models/customer');
async function getCustomer(req,res,next) {
    let thisCustomer;
    try {
        thisCustomer = await Customer.findById(req.params.id).exec();
        if(thisCustomer === null) {
            return res.status(404).json({message : `Couldnt find customer with id ${req.params.id}`});
        }
    } catch(err) {
        return res.status(500).json({message : err.message});
    }
    res.custy = thisCustomer;
    next();
}
//export middleware func
module.exports = getCustomer;