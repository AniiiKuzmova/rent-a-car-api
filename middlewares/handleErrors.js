//handle error function
const handleErrors = (error) => {
    let errors = {};
    //validation errors
       Object.values(error.errors).forEach(({properties}) => {
           errors[properties.path] = properties.message;
       });
    //return errors object
    return errors;
};
//export function
module.exports = handleErrors;