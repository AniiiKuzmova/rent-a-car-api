const jwt = require('jsonwebtoken');
//authenticate user/customer function
const requireAuth = (req,res,next) => {
    //grab the token
    const token = req.cookies.jwt;
    console.log(token)
    //check token is set
    if(token) {
        jwt.verify(token, 'ani', (err, decodedToken) => {
            if(err) {
                res.json(err);
            }else {
                next();
            }
        });
    }else {
        res.status(401).json('Not logged or registered.');
    }
}

module.exports = {requireAuth};