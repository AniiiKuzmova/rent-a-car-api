const VehicleType = require('../models/vehicleType');
//middleware function for getting vehicleType with specific id
async function getVehicleType(req,res,next) {
    let vTypes;
    try {
        vTypes = await VehicleType.findById(req.params.id).exec();
        if(vTypes === null) {
            return res.status(404).json({message : `Couldnt find vehicle type with id ${req.params.id}`});
        }
    }catch(err) {
        return res.status(500).json({message : err.message});
    }
    res.vehicleType = vTypes;
    next();
}
//export middleware function
module.exports = getVehicleType;