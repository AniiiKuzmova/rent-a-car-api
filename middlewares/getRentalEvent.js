const RentalEvent = require('../models/rentalEvent');
//middleware function for getting single rental event
async function getRentalEvent(req,res,next) {
    let rentalEvent;
    try{
        rentalEvent = await RentalEvent.findById(req.params.id).exec();
        if(rentalEvent === null) {
            return res.status(404).json({message: `Could not find rental event with id ${req.params.id}`});
        }
    }catch(error) {
        return res.status(500).json({message: error.message});
    }
    res.foundRentalEvent = rentalEvent;
    next();
};
//export middleware function
module.exports = getRentalEvent;