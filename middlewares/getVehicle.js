const Vehicle = require('../models/vehicle');
//middlesware functiong for getting a specific vehicle
async function getVehicle(req,res,next) {
    let vehicle;
    try{
         vehicle = await Vehicle.findById(req.params.id).exec();
         if(vehicle == null) {
            return res.status(404).json({message: `Could not find vehicle with id : ${req.params.id}`});
         }
    }catch(error) {
        return res.status(500).json({message: error});
    }
    res.foundVehicle = vehicle;
    next(); 
}
//export the middleware function
module.exports = getVehicle; 