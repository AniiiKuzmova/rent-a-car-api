const Vehicle = require('../models/vehicle');
const RentalEvent = require('../models/rentalEvent');
const Customer = require('../models/customer')
const mongoose = require('mongoose');
//function for calculating the range between two dates
function calculateDateBtweenDates(date1,date2) {
    const date1Format = new Date(date1);
    const date2Format = new Date(date2);
    return parseInt((date1Format - date2Format) / (1000 * 60 * 60 * 24), 10);
}
//calculate the price 
async function calculatePrice(dateRange,customer,vehicle) {
    // //check if vehicle exists
    let discount = 0;
    // //check the rental period
    if(dateRange > 3 && dateRange <=5) {
         discount = (vehicle.pricePerDay * 5)/100;
    }else if(dateRange > 5 && dateRange <= 10) {
         discount = (vehicle.pricePerDay * 7)/100;
    }else if(dateRange > 10){
         discount = (vehicle.pricePerDay * 10)/100;
     }
    // //check if clent is VIP client(client has rented more than 3 times in the last 60 days.)
    // //create date format for last 60 days
    const now = new Date();
    const last60days = new Date(now.setDate(now.getDate() - 60));
    // // //find all purchases count for specific customer made in the last 60 days
    const purchases = await mongoose.models.Rentalevent.find({"customer": customer._id,"startDate" : {"$gt": last60days}}).countDocuments().exec();
    // //check if purchases are more than three
    if(purchases > 3) {
        discount = (vehicle.pricePerDay * 15)/100;
    }
     //substract discount from price and return price
    return vehicle.pricePerDay - discount;
}
module.exports = {calculateDateBtweenDates,calculatePrice}
