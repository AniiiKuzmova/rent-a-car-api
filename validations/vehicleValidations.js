const mongoose = require('mongoose');
const VehicleType = require('../models/vehicleType');
//check if fuel type is diesel,petrol,electric or hybrid
function validateFuelType(fuelType) {
    const fuelTypeValues = ['diesel', 'petrol', 'hybrid', 'electric'];
    if(fuelTypeValues.includes(fuelType)) {
        return true;
    }
    return false;
}
//check if vehicle type exists
async function checkVehicleType(vehicleType) {
    let vehicleTypeDocs;
        vehicleTypeDocs = await VehicleType.findById(vehicleType).countDocuments().exec();
        if(vehicleTypeDocs === 0) {
            return false;
        }
    return true;
}
//export validation functions
module.exports = {validateFuelType,checkVehicleType};