const mongoose = require('mongoose');
const Customer = require('../models/customer');
const Vehicle = require('../models/vehicle');
//check if vehicle with the given id exists
async function checkVehicle(value) {
    let vehicleCount;
        vehicleCount = await Vehicle.findById(value).exec();
        if(vehicleCount === null) {
            return false;
        }
    return true;
}
//check if customer with given id exists
async function checkCustomer(value) {
    let customerCount;
        customerCount = await Customer.findById(value).exec();
        if(customerCount === null) {
           return false;
        }
    return true;
}
//Check if user has correctly set dates
function validateDate(value) {
    if(value >= this.startDate) {
        return true;
    }
    return false;
}
//export validation functions
module.exports = {checkCustomer,validateDate,checkVehicle};